package com.controllers;

import com.commons.dtos.MedicationPlanDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.services.MedicationPlanService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans() {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlans();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedicationPlan(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) {
        UUID medicationPlanID = medicationPlanService.insertMedicationPlan(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlanID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationPlanDTO> getMedicationById(@PathVariable("id") UUID medicationPlanId) {
        MedicationPlanDTO dto = medicationPlanService.findMedicationPlanById(medicationPlanId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MedicationPlanDTO> deleteMedication(@PathVariable("id") UUID medicationPlanId){
        MedicationPlanDTO dto = medicationPlanService.deleteMedicationPlanById(medicationPlanId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/addMedicationLink/{idMedicationPlan}/{medicationName}")
    public ResponseEntity<UUID> addMedicationToMedicationPlan(@PathVariable("idMedicationPlan") UUID medicationPlanId,
                                                              @PathVariable("medicationName") String medicationName,
                                                              @RequestBody String dailyInterval) {
        medicationPlanService.addMedicationToMedicationPlan(medicationName, medicationPlanId, dailyInterval);
        return new ResponseEntity<>(medicationPlanId, HttpStatus.OK);
    }
}

