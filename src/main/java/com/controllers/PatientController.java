package com.controllers;

import com.commons.dtos.PatientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.services.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = patientService.findPatients();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/patientNames")
    public ResponseEntity<List<String>> getPatientNames() {
        List<String> patientNames = patientService.findPatientsNames();
        return new ResponseEntity<>(patientNames, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertPatient(@Valid @RequestBody PatientDTO patientDTO) {
        UUID patientID = patientService.insertPatient(patientDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> getPatient(@PathVariable("id") UUID patientId) {
        PatientDTO dto = patientService.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> deletePatient(@PathVariable("id") UUID patientId){
        PatientDTO dto = patientService.deletePatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> updatePatientById(@PathVariable("id") UUID patientId,
                                                        @Valid @RequestBody PatientDTO patientDTO) {
        PatientDTO dto = patientService.updatePatientById(patientId, patientDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/addPlan/{patientName}/{idMedicationPlan}")
    public ResponseEntity<UUID> addMedicationPlanToPatient(@PathVariable("patientName") String patientName,
                                                           @PathVariable("idMedicationPlan") UUID medicationPlanId) {
        patientService.addMedicationPlanToPatientByPatientName(medicationPlanId, patientName);
        return new ResponseEntity<>(medicationPlanId, HttpStatus.OK);
    }

    @PutMapping(value = "/addIntake/{patientName}/{idMedicationIntake}")
    public ResponseEntity<UUID> addMedicationIntakeToPatient(@PathVariable("patientName") String patientName,
                                                           @PathVariable("idMedicationIntake") UUID medicationIntakeId) {
        patientService.addMedicationIntakeToPatientByPatientName(medicationIntakeId, patientName);
        return new ResponseEntity<>(medicationIntakeId, HttpStatus.OK);
    }

    @PutMapping(value = "/addIntakeById/{patientId}/{idMedicationIntake}")
    public ResponseEntity<UUID> addMedicationIntakeToPatientById(@PathVariable("patientId") UUID patientId,
                                                             @PathVariable("idMedicationIntake") UUID medicationIntakeId) {
        patientService.addMedicationIntakeToPatient(medicationIntakeId, patientId);
        return new ResponseEntity<>(medicationIntakeId, HttpStatus.OK);
    }


}
