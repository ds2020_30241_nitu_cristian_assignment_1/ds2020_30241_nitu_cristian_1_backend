package com.controllers;

import com.commons.dtos.MedicationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.services.MedicationService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findMedications();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        UUID medicationID = medicationService.insertMedication(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable("id") UUID medicationId) {
        MedicationDTO dto = medicationService.findMedicationById(medicationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> deleteMedication(@PathVariable("id") UUID medicationId){
        MedicationDTO dto = medicationService.deleteMedicationById(medicationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> updateMedicationById(@PathVariable("id") UUID medicationId,
                                                              @Valid @RequestBody MedicationDTO medicationDTO) {
        MedicationDTO dto = medicationService.updateMedicationById(medicationId, medicationDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/medicationNames")
    public ResponseEntity<List<String>> getMedicationNames() {
        List<String> medicationNames = medicationService.findMedicationNames();
        return new ResponseEntity<>(medicationNames, HttpStatus.OK);
    }
}
