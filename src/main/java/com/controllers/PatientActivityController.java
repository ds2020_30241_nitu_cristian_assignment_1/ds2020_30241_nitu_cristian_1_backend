package com.controllers;

import com.commons.dtos.PatientActivityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.services.PatientActivityService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/activity")
public class PatientActivityController {

    private final PatientActivityService patientActivityService;

    @Autowired
    public PatientActivityController(PatientActivityService patientActivityService) {
        this.patientActivityService = patientActivityService;
    }

    @GetMapping()
    public ResponseEntity<List<PatientActivityDTO>> getActivities() {
        List<PatientActivityDTO> dtos = patientActivityService.findActivities();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertPatientActivity(@Valid @RequestBody PatientActivityDTO patientActivityDTO) {
        UUID patientActivityID = patientActivityService.insertActivity(patientActivityDTO);
        return new ResponseEntity<>(patientActivityID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientActivityDTO> getPatientActivity(@PathVariable("id") UUID patientActivityId) {
        PatientActivityDTO dto = patientActivityService.findActivityById(patientActivityId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<PatientActivityDTO> deletePatientActivity(@PathVariable("id") UUID patientActivityId){
        PatientActivityDTO dto = patientActivityService.deleteActivityById(patientActivityId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
