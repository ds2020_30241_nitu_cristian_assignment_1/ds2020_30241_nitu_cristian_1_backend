package com.controllers;

import com.commons.dtos.CaregiverDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.services.CaregiverService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregivers() {
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO) {
        UUID caregiverID = caregiverService.insertCaregiver(caregiverDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/caregiverNames")
    public ResponseEntity<List<String>> getCaregiverNames() {
        List<String> caregiverNames = caregiverService.findCaregiversNames();
        return new ResponseEntity<>(caregiverNames, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("id") UUID caregiverId) {
        CaregiverDTO dto = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> deleteCaregiver(@PathVariable("id") UUID caregiverId){
        CaregiverDTO dto = caregiverService.deleteCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/addPatient/{caregiverName}/{patientName}")
    public ResponseEntity<String> addPatientToCaregiver(@PathVariable("caregiverName") String caregiverName,
                                                      @PathVariable("patientName") String patientName) {
        caregiverService.addPatientToCaregiverString(patientName, caregiverName);
        return new ResponseEntity<>(caregiverName, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> updateCaregiverById(@PathVariable("id") UUID caregiverId,
                                                        @Valid @RequestBody CaregiverDTO caregiverDTO) {
        CaregiverDTO dto = caregiverService.updateCaregiverById(caregiverId, caregiverDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
