package com.controllers;

import com.commons.dtos.MedicationIntakeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.services.MedicationIntakeService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationIntake")
public class MedicationIntakeController {

    private final MedicationIntakeService medicationIntakeService;

    @Autowired
    public MedicationIntakeController(MedicationIntakeService medicationIntakeService) {
        this.medicationIntakeService = medicationIntakeService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationIntakeDTO>> getMedicationIntake() {
        List<MedicationIntakeDTO> dtos = medicationIntakeService.findMedicationIntakes();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedicationIntake(@Valid @RequestBody MedicationIntakeDTO medicationIntakeDTO) {
        UUID medicationIntakeID = medicationIntakeService.insertMedicationIntake(medicationIntakeDTO);
        return new ResponseEntity<>(medicationIntakeID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationIntakeDTO> getMedicationIntakeById(@PathVariable("id") UUID medicationIntakeId) {
        MedicationIntakeDTO dto = medicationIntakeService.findMedicationIntakeById(medicationIntakeId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MedicationIntakeDTO> deleteMedicationIntake(@PathVariable("id") UUID medicationIntakeId){
        MedicationIntakeDTO dto = medicationIntakeService.deleteMedicationIntakeById(medicationIntakeId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}

