package com.services;

import com.builders.MedicationBuilder;
import com.commons.dtos.MedicationDTO;
import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.entities.Medication;
import com.repositories.MedicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public List<MedicationDTO> findMedications() {
        List<Medication> medicationsList = medicationRepository.findAll();
        return medicationsList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(UUID medicationId) {
        Optional<Medication> prosumerOptionalMedication = medicationRepository.findById(medicationId);
        if (!prosumerOptionalMedication.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", medicationId);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + medicationId);
        }
        return MedicationBuilder.toMedicationDTO(prosumerOptionalMedication.get());
    }

    public UUID insertMedication(MedicationDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getMedicationId());
        return medication.getMedicationId();
    }

    public MedicationDTO deleteMedicationById(UUID medicationId){
        Optional<Medication> prosumerOptionalMedication = medicationRepository.findById(medicationId);
        if (!prosumerOptionalMedication.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", medicationId);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + medicationId);
        }

        medicationRepository.deleteById(medicationId);
        return MedicationBuilder.toMedicationDTO(prosumerOptionalMedication.get());
    }

    public MedicationDTO updateMedicationById(UUID medicationId, MedicationDTO medicationDTO){
        Optional<Medication> prosumerOptionalMedication = medicationRepository.findById(medicationId);
        if (!prosumerOptionalMedication.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", medicationId);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + medicationId);
        }

        Medication medication = prosumerOptionalMedication.get();
        Medication newMedication = MedicationBuilder.toEntity(medicationDTO);

        if(medicationDTO.getName().length() > 0){
            medication.setName(newMedication.getName());
        }

        if(medicationDTO.getDosage() > 0){
            medication.setDosage(newMedication.getDosage());
        }

        if(medicationDTO.getSideEffect().length() > 0){
            medication.setSideEffect(newMedication.getSideEffect());
        }

        medicationRepository.save(medication);

        return MedicationBuilder.toMedicationDTO(medication);

    }

    public List<String> findMedicationNames() {
        List<Medication> medicationList = medicationRepository.findAll();
        List<String> medicationNames = new ArrayList<>();

        for(Medication medication: medicationList){
            medicationNames.add(medication.getName());
        }

        return medicationNames;
    }
}
