package com.services;

import com.builders.MedicationIntakeBuilder;
import com.commons.dtos.MedicationIntakeDTO;
import com.commons.dtos.MedicationPlanDTO;
import com.entities.MedicationIntake;
import com.entities.MedicationPlan;
import com.entities.Patient;
import com.repositories.MedicationIntakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Component;
import com.commons.IPatientService;
import com.builders.MedicationPlanBuilder;
import com.repositories.PatientRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class PatientServiceImpl implements IPatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private MedicationIntakeRepository medicationIntakeRepository;

    @Override
    public Set<MedicationPlanDTO> findPlansByPatientId(UUID id) {
        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(id);

        if (!prosumerOptionalPatient.isPresent()) {
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }

        Patient patient = prosumerOptionalPatient.get();

        Set<MedicationPlan> medicationPlans;
        medicationPlans = patient.getMedicationPlans();

        Set<MedicationPlanDTO> medicationPlanDTOS = new HashSet<>();

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        Date crtDateUnformatted = new Date(System.currentTimeMillis());
        String date = sdf.format(crtDateUnformatted);

        for(MedicationPlan medicationPlan: medicationPlans) {
            String startDate = medicationPlan.getStartTreatment();
            String endDate = medicationPlan.getEndTreatment();

            try {
                Date crtDate = sdf.parse(date);
                Date start = sdf.parse(startDate);
                Date end = sdf.parse(endDate);

                if(start.before(crtDate) && crtDate.before(end)) {
                    medicationPlanDTOS.add(MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        return medicationPlanDTOS;
    }


    @Override
    public void takeMedication(MedicationIntakeDTO medicationIntakeDTO, UUID patientId) {
        MedicationIntake medicationIntake = MedicationIntakeBuilder.toEntity(medicationIntakeDTO);
        medicationIntake = medicationIntakeRepository.save(medicationIntake);

        Patient patient = patientRepository.findById(patientId).get();

        patient.addMedicationIntake(medicationIntake);
        patientRepository.save(patient);
    }


}


