package com.services;

import com.builders.PatientBuilder;
import com.commons.dtos.PatientDTO;
import com.commons.dtos.PatientDTOWithCaregiver;
import com.entities.MedicationIntake;
import com.entities.MedicationPlan;
import com.entities.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.repositories.MedicationIntakeRepository;
import com.repositories.MedicationPlanRepository;
import com.repositories.PatientRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationIntakeRepository medicationIntakeRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository,
                          MedicationPlanRepository medicationPlanRepository,
                          MedicationIntakeRepository medicationIntakeRepository) {
        this.patientRepository = patientRepository;
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationIntakeRepository = medicationIntakeRepository;
    }

    public List<PatientDTO> findPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public List<String> findPatientsNames() {
        List<Patient> patientList = patientRepository.findAll();
        List<String> patientNames = new ArrayList<>();

        for(Patient patient: patientList){
            patientNames.add(patient.getName());
        }

        return patientNames;
    }


    public List<PatientDTOWithCaregiver> findPatientsWithCaregiver() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTOWithCaregiver)
                .collect(Collectors.toList());
    }

    public PatientDTO findPatientById(UUID patientId) {
        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(patientId);
        if (!prosumerOptionalPatient.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", patientId);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }
        return PatientBuilder.toPatientDTO(prosumerOptionalPatient.get());
    }

    public UUID insertPatient(PatientDTO patientDTO) {
        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getUserId());
        return patient.getUserId();
    }

    public PatientDTO deletePatientById(UUID patientId){
        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(patientId);
        if (!prosumerOptionalPatient.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", patientId);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }

        patientRepository.deleteById(patientId);
        return PatientBuilder.toPatientDTO(prosumerOptionalPatient.get());
    }

    public PatientDTO updatePatientById(UUID patientId, PatientDTO patientDTO){
        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(patientId);
        if (!prosumerOptionalPatient.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", patientId);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }

        Patient patient = prosumerOptionalPatient.get();
        Patient newPatient = PatientBuilder.toEntity(patientDTO);

        if(newPatient.getUsername().length() == 0){
            patient.setUsername(patient.getUsername());
        }

        if(newPatient.getPassword().length() == 0){
            patient.setPassword(patient.getPassword());
        }

        if(newPatient.getName().length() == 0){
            patient.setName(patient.getName());
        }

        if(newPatient.getBirthdate().length() == 0){
            patient.setBirthdate(patient.getBirthdate());
        }

        if(newPatient.getGender().length() == 0){
            patient.setGender(patient.getGender());
        }

        if(newPatient.getAddress().length() > 0){
            patient.setAddress(newPatient.getAddress());
        }
        if(newPatient.getMedicalRecord().length() > 0){
            patient.setMedicalRecord(newPatient.getMedicalRecord());
        }
        patientRepository.save(patient);

        return PatientBuilder.toPatientDTO(patient);

    }

    public void addMedicationPlanToPatient(UUID medicationPlanId, UUID patientId) {
        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(patientId);
        Optional<MedicationPlan> prosumerOptionalMedicationPlan =
                medicationPlanRepository.findById(medicationPlanId);

        if (!prosumerOptionalMedicationPlan.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", medicationPlanId);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationPlanId);
        }

        if (!prosumerOptionalPatient.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", patientId);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }

        prosumerOptionalPatient.get().addMedicationPlan(prosumerOptionalMedicationPlan.get());
        patientRepository.save(prosumerOptionalPatient.get());
    }

    public void addMedicationPlanToPatientByPatientName(UUID medicationPlanId, String patientName) {
        Optional<MedicationPlan> prosumerOptionalMedicationPlan =
                medicationPlanRepository.findById(medicationPlanId);

        if (!prosumerOptionalMedicationPlan.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", medicationPlanId);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationPlanId);
        }

        Patient patient = patientRepository.findPatientByName(patientName);

        patient.addMedicationPlan(prosumerOptionalMedicationPlan.get());
        patientRepository.save(patient);
    }

    public void addMedicationIntakeToPatientByPatientName(UUID medicationIntakeId, String patientName) {
        Optional<MedicationIntake> prosumerOptionalMedicationIntake =
                medicationIntakeRepository.findById(medicationIntakeId);

        if (!prosumerOptionalMedicationIntake.isPresent()) {
            LOGGER.error("Medication intake with id {} was not found in db", medicationIntakeId);
            throw new ResourceNotFoundException(MedicationIntake.class.getSimpleName() + " with id: " + medicationIntakeId);
        }

        Patient patient = patientRepository.findPatientByName(patientName);

        patient.addMedicationIntake(prosumerOptionalMedicationIntake.get());
        patientRepository.save(patient);
    }

    public void addMedicationIntakeToPatient(UUID medicationIntakeId, UUID patientId) {
        Optional<MedicationIntake> prosumerOptionalMedicationIntake =
                medicationIntakeRepository.findById(medicationIntakeId);

        if (!prosumerOptionalMedicationIntake.isPresent()) {
            LOGGER.error("Medication intake with id {} was not found in db", medicationIntakeId);
            throw new ResourceNotFoundException(MedicationIntake.class.getSimpleName() + " with id: " + medicationIntakeId);
        }

        Patient patient = patientRepository.findById(patientId).get();

        patient.addMedicationIntake(prosumerOptionalMedicationIntake.get());
        patientRepository.save(patient);
    }

}
