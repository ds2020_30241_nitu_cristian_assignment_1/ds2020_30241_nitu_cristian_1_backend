package com.services;

import com.commons.dtos.MedicationIntakeDTO;
import com.entities.MedicationIntake;
import com.entities.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.commons.IMedicationIntakeService;
import com.builders.MedicationIntakeBuilder;
import com.repositories.MedicationIntakeRepository;
import com.repositories.PatientRepository;

import java.util.UUID;

@Component
public class MedicationIntakeServiceImpl implements IMedicationIntakeService {

    @Autowired
    private MedicationIntakeRepository medicationIntakeRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void takeMedication(MedicationIntakeDTO medicationIntakeDTO, UUID patientId) {
        MedicationIntake medicationIntake = MedicationIntakeBuilder.toEntity(medicationIntakeDTO);
        medicationIntake = medicationIntakeRepository.save(medicationIntake);

        Patient patient = patientRepository.findById(patientId).get();

        patient.addMedicationIntake(medicationIntake);
        patientRepository.save(patient);
    }
}
