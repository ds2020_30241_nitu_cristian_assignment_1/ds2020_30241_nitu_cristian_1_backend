package com.services;

import com.commons.dtos.MedicationIntakeDTO;
import com.entities.MedicationIntake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.builders.MedicationIntakeBuilder;
import com.repositories.MedicationIntakeRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationIntakeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationIntakeRepository medicationIntakeRepository;

    @Autowired
    public MedicationIntakeService(MedicationIntakeRepository medicationIntakeRepository) {
        this.medicationIntakeRepository = medicationIntakeRepository;
    }

    public List<MedicationIntakeDTO> findMedicationIntakes() {
        List<MedicationIntake> medicationIntakeList = medicationIntakeRepository.findAll();
        return medicationIntakeList.stream()
                .map(MedicationIntakeBuilder::toMedicationIntakeDTO)
                .collect(Collectors.toList());
    }

    public MedicationIntakeDTO findMedicationIntakeById(UUID medicationIntakeId) {
        Optional<MedicationIntake> prosumerOptionalMedicationIntake = medicationIntakeRepository.findById(medicationIntakeId);
        if (!prosumerOptionalMedicationIntake.isPresent()) {
            LOGGER.error("Medication intake with id {} was not found in db", medicationIntakeId);
            throw new ResourceNotFoundException(MedicationIntake.class.getSimpleName() + " with id: " + medicationIntakeId);
        }
        return MedicationIntakeBuilder.toMedicationIntakeDTO(prosumerOptionalMedicationIntake.get());
    }

    public UUID insertMedicationIntake(MedicationIntakeDTO medicationIntakeDTO) {
        MedicationIntake medicationIntake = MedicationIntakeBuilder.toEntity(medicationIntakeDTO);
        medicationIntake = medicationIntakeRepository.save(medicationIntake);
        LOGGER.debug("Medication intake with id {} was inserted in db", medicationIntake.getMedicationIntakeId());
        return medicationIntake.getMedicationIntakeId();
    }

    public MedicationIntakeDTO deleteMedicationIntakeById(UUID medicationIntakeId){
        Optional<MedicationIntake> prosumerOptionalMedicationIntake = medicationIntakeRepository.findById(medicationIntakeId);
        if (!prosumerOptionalMedicationIntake.isPresent()) {
            LOGGER.error("Medication intake with id {} was not found in db", medicationIntakeId);
            throw new ResourceNotFoundException(MedicationIntake.class.getSimpleName() + " with id: " + medicationIntakeId);
        }

        medicationIntakeRepository.deleteById(medicationIntakeId);
        return MedicationIntakeBuilder.toMedicationIntakeDTO(prosumerOptionalMedicationIntake.get());
    }

}
