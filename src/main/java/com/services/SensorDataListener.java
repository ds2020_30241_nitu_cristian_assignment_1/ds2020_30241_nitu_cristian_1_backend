package com.services;

import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.entities.Patient;
import com.entities.PatientActivity;
import com.entities.RuleViolation;
import com.repositories.PatientActivityRepository;
import com.repositories.PatientRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Optional;
import java.util.UUID;

@Service
public class SensorDataListener {
    private static final Logger log = LoggerFactory.getLogger(SensorDataListener.class);
    public static final String DEFAULT_PARSING_QUEUE = "default_parser_q3";
    public static final String DEFAULT_PARSING_QUEUE2 = "default_parser_q4";

    public static String messageString = "";
    private final SimpMessagingTemplate simpMessagingTemplate;
    private ParseDataService parseDataService = new ParseDataService();
    private final PatientActivityRepository patientActivityRepository;
    private final PatientRepository patientRepository;

    int count = 0;

    @Autowired
    public SensorDataListener(PatientActivityRepository patientActivityRepository,
                              PatientRepository patientRepository,
                              SimpMessagingTemplate simpMessagingTemplate) {
        this.patientActivityRepository = patientActivityRepository;
        this.patientRepository = patientRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public UUID getPatientCaregiver(String patientId) {
        UUID patientIdUUID = UUID.fromString(patientId);
        Optional<Patient> patient = patientRepository.findById(patientIdUUID);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }

        UUID caregiverId = patient.get().getCaregiver().getUserId();

        return caregiverId;
    }

    public String getPatientUsername(String patientId) {
        UUID patientIdUUID = UUID.fromString(patientId);
        Optional<Patient> patient = patientRepository.findById(patientIdUUID);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }

        return patient.get().getUsername();
    }

    @RabbitListener(queues = DEFAULT_PARSING_QUEUE)
    public void consumeDefaultMessage(final Message message2) throws UnsupportedEncodingException, JSONException {
        String message = new String((byte[]) message2.getPayload(), "UTF-8");

        JSONObject testV=new JSONObject(message);

        PatientActivity patientActivity = new PatientActivity(testV.getString("startActivity"),
                testV.getString("endActivity"), testV.getString("activity"));
        this.patientActivityRepository.save(patientActivity);

        UUID uid = UUID.fromString(testV.getString("patientId"));
        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(uid);

        if (!prosumerOptionalPatient.isPresent()) {
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + uid);
        }

        prosumerOptionalPatient.get().addPatientActivity(patientActivity);
        patientRepository.save(prosumerOptionalPatient.get());
    }

    @RabbitListener(queues = DEFAULT_PARSING_QUEUE2)
    public void consumeDefaultMessage2(final Message message2) throws UnsupportedEncodingException, JSONException {
        String message = new String((byte[]) message2.getPayload(), "UTF-8");

        JSONObject testV = new JSONObject(message);

        PatientActivity patientActivity = new PatientActivity(testV.getString("startActivity"),
                testV.getString("endActivity"), testV.getString("activity"));

        RuleViolation ruleViolation = null;
        boolean isViolated = false;

        long time = 0;
        try {
            time = parseDataService.getTime(patientActivity.getStart(), patientActivity.getEnd());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }

        UUID caregiverId = this.getPatientCaregiver(testV.getString("patientId"));

        if(patientActivity.getActivity().equals("Sleeping") && time > 25200){
            SensorDataListener.messageString = "Sleep period is longer than 7 hours for";
            isViolated = true;
            try {
                ruleViolation = new RuleViolation(testV.getString("startActivity"), testV.getString("endActivity"),
                        testV.getString("activity"), testV.getString("patientId"), caregiverId.toString(),
                        this.getPatientUsername(testV.getString("patientId")), SensorDataListener.messageString);
            } catch (JSONException e) {
                System.out.println(e.getMessage());
            }
        }

        else if(patientActivity.getActivity().equals("Leaving") && time > 18000){
            SensorDataListener.messageString = "The leaving activity (outdoor) is longer than 5 hours for";
            isViolated = true;
            try {
                ruleViolation = new RuleViolation(testV.getString("startActivity"), testV.getString("endActivity"),
                        testV.getString("activity"), testV.getString("patientId"), caregiverId.toString(),
                        this.getPatientUsername(testV.getString("patientId")), SensorDataListener.messageString);
            } catch (JSONException e) {
                System.out.println(e.getMessage());
            }
        }

        else if((patientActivity.getActivity().equals("Toileting") || (patientActivity.getActivity().equals("Showering")))
                && time > 1800){
            SensorDataListener.messageString = "Period spent in bathroom is longer than 30 minutes for";
            isViolated = true;
            try {
                ruleViolation = new RuleViolation(testV.getString("startActivity"), testV.getString("endActivity"),
                        testV.getString("activity"), testV.getString("patientId"), caregiverId.toString(),
                        this.getPatientUsername(testV.getString("patientId")), SensorDataListener.messageString);
            } catch (JSONException e) {
                System.out.println(e.getMessage());
            }

        }

        else {
            SensorDataListener.messageString = "";
        }

        if(isViolated == true) {
            simpMessagingTemplate.convertAndSend("/topic/violations", ruleViolation);
        }



    }

}
