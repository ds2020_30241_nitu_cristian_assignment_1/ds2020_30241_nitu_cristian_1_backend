package com.services;

import com.commons.dtos.DoctorDTO;
import com.commons.dtos.DoctorDTOWithoutUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.builders.DoctorBuilder;
import com.entities.Doctor;
import com.repositories.DoctorRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);
    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public List<DoctorDTOWithoutUserDetails> findDoctors() {
        List<Doctor> doctorList = doctorRepository.findAll();
        return doctorList.stream()
                .map(DoctorBuilder::toDoctorDTOWithoutDetails)
                .collect(Collectors.toList());
    }

    public DoctorDTO findDoctorById(UUID doctorId) {
        Optional<Doctor> prosumerOptionalDoctor = doctorRepository.findById(doctorId);
        if (!prosumerOptionalDoctor.isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db", doctorId);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + doctorId);
        }
        return DoctorBuilder.toDoctorDTO(prosumerOptionalDoctor.get());
    }

    public UUID insertDoctor(DoctorDTO doctorDTO) {
        Doctor doctor = DoctorBuilder.toEntity(doctorDTO);
        doctor = doctorRepository.save(doctor);
        LOGGER.debug("Doctor with id {} was inserted in db", doctor.getUserId());
        return doctor.getUserId();
    }

    public DoctorDTO deleteDoctorById(UUID doctorId){
        Optional<Doctor> prosumerOptionalDoctor = doctorRepository.findById(doctorId);

        if (!prosumerOptionalDoctor.isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db", doctorId);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + doctorId);
        }

        doctorRepository.deleteById(doctorId);
        return DoctorBuilder.toDoctorDTO(prosumerOptionalDoctor.get());
    }

}
