package com.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import com.commons.IMedicationIntakeService;
import com.commons.IPatientService;

@Configuration
public class Server  {

    @Autowired
    PatientServiceImpl patientService;


    @Bean(name = "/plans")
    HessianServiceExporter getPlans() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(patientService);
        exporter.setServiceInterface( IPatientService.class );
        return exporter;
    }
/*
    @Bean(name = "/medicationTaken")
    RemoteExporter takeMedication() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(medicationIntakeService);
        exporter.setServiceInterface( IMedicationIntakeService.class );
        return exporter;
    }
*/
}