package com.services;

import com.commons.dtos.PatientActivityDTO;
import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.entities.PatientActivity;
import com.repositories.PatientActivityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.builders.PatientActivityBuilder;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientActivityService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final PatientActivityRepository patientActivityRepository;

    @Autowired
    public PatientActivityService(PatientActivityRepository patientActivityRepository) {
        this.patientActivityRepository = patientActivityRepository;
    }

    public List<PatientActivityDTO> findActivities() {
        List<PatientActivity> patientActivityList = patientActivityRepository.findAll();
        return patientActivityList.stream()
                .map(PatientActivityBuilder::toPatientActivityDTO)
                .collect(Collectors.toList());
    }

    public PatientActivityDTO findActivityById(UUID patientActivityId) {
        Optional<PatientActivity> prosumerOptionalActivity = patientActivityRepository.findById(patientActivityId);
        if (!prosumerOptionalActivity.isPresent()) {
            LOGGER.error("Activity with id {} was not found in db", patientActivityId);
            throw new ResourceNotFoundException(PatientActivity.class.getSimpleName() + " with id: " + patientActivityId);
        }
        return PatientActivityBuilder.toPatientActivityDTO(prosumerOptionalActivity.get());
    }

    public UUID insertActivity(PatientActivityDTO patientActivityDTO) {
        PatientActivity patientActivity = PatientActivityBuilder.toEntity(patientActivityDTO);
        patientActivity = patientActivityRepository.save(patientActivity);
        LOGGER.debug("Activity with id {} was inserted in db", patientActivity.getActivityId());
        return patientActivity.getActivityId();
    }

    public PatientActivityDTO deleteActivityById(UUID patientActivityId){
        Optional<PatientActivity> prosumerOptionalPatientActivity = patientActivityRepository.findById(patientActivityId);
        if (!prosumerOptionalPatientActivity.isPresent()) {
            LOGGER.error("Activity with id {} was not found in db", patientActivityId);
            throw new ResourceNotFoundException(PatientActivity.class.getSimpleName() + " with id: " + patientActivityId);
        }

        patientActivityRepository.deleteById(patientActivityId);
        return PatientActivityBuilder.toPatientActivityDTO(prosumerOptionalPatientActivity.get());
    }
}
