package com.services;

import com.commons.dtos.CaregiverDTO;
import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.entities.Caregiver;
import com.entities.Patient;
import com.repositories.CaregiverRepository;
import com.repositories.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.builders.CaregiverBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, PatientRepository patientRepository) {
        this.caregiverRepository = caregiverRepository;
        this.patientRepository = patientRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public List<String> findCaregiversNames() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        List<String> caregiverNames = new ArrayList<>();

        for(Caregiver caregiver: caregiverList){
            caregiverNames.add(caregiver.getName());
        }

        return caregiverNames;
    }

    public CaregiverDTO findCaregiverById(UUID caregiverId) {
        Optional<Caregiver> prosumerOptionalCaregiver = caregiverRepository.findById(caregiverId);
        if (!prosumerOptionalCaregiver.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverId);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + caregiverId);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptionalCaregiver.get());
    }

    public UUID insertCaregiver(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getUserId());
        return caregiver.getUserId();
    }

    public CaregiverDTO deleteCaregiverById(UUID caregiverId){
        Optional<Caregiver> prosumerOptionalCaregiver = caregiverRepository.findById(caregiverId);
        if (!prosumerOptionalCaregiver.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverId);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + caregiverId);
        }

        caregiverRepository.deleteById(caregiverId);
        return CaregiverBuilder.toCaregiverDTO(prosumerOptionalCaregiver.get());
    }

    public void addPatientToCaregiver(UUID patientId, UUID caregiverId) {
        Optional<Caregiver> prosumerOptionalCaregiver = caregiverRepository.findById(caregiverId);
        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(patientId);

        if (!prosumerOptionalCaregiver.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverId);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + caregiverId);
        }

        if (!prosumerOptionalPatient.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", patientId);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }

        prosumerOptionalCaregiver.get().addPatient(prosumerOptionalPatient.get());
        caregiverRepository.save(prosumerOptionalCaregiver.get());
    }

    public void addPatientToCaregiverString(String patientName, String caregiverName) {
        Caregiver caregiver = caregiverRepository.findCaregiverByName(caregiverName);
        Patient patient = patientRepository.findPatientByName(patientName);

        caregiver.addPatient(patient);
        caregiverRepository.save(caregiver);
    }

    public CaregiverDTO updateCaregiverById(UUID caregiverId, CaregiverDTO caregiverDTO){
        Optional<Caregiver> prosumerOptionalCaregiver = caregiverRepository.findById(caregiverId);
        if (!prosumerOptionalCaregiver.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverId);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + caregiverId);
        }

        Caregiver caregiver = prosumerOptionalCaregiver.get();
        Caregiver newCaregiver = CaregiverBuilder.toEntity(caregiverDTO);

        if(newCaregiver.getUsername().length() == 0){
            caregiver.setUsername(caregiver.getUsername());
        }

        if(newCaregiver.getPassword().length() == 0){
            caregiver.setPassword(caregiver.getPassword());
        }

        if(newCaregiver.getName().length() == 0){
            caregiver.setName(caregiver.getName());
        }

        if(newCaregiver.getBirthdate().length() == 0){
            caregiver.setBirthdate(caregiver.getBirthdate());
        }

        if(newCaregiver.getGender().length() == 0){
            caregiver.setGender(caregiver.getGender());
        }

        if(newCaregiver.getAddress().length() > 0){
            caregiver.setAddress(newCaregiver.getAddress());
        }

        caregiverRepository.save(caregiver);

        return CaregiverBuilder.toCaregiverDTO(caregiver);

    }

}
