package com.services;

import com.commons.dtos.MedicationPlanDTO;
import com.entities.MedicationPlan;
import com.entities.MedicationPlanToMedication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.builders.MedicationPlanBuilder;
import com.entities.Medication;
import com.repositories.MedicationPlanRepository;
import com.repositories.MedicationPlanToMedicationRepository;
import com.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;
    private final MedicationPlanToMedicationRepository medicationPlanToMedicationRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository,
                                 MedicationRepository medicationRepository,
                                 MedicationPlanToMedicationRepository medicationPlanToMedicationRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;
        this.medicationPlanToMedicationRepository = medicationPlanToMedicationRepository;
    }

    public List<MedicationPlanDTO> findMedicationPlans() {
        List<MedicationPlan> medicationPlansList = medicationPlanRepository.findAll();
        return medicationPlansList.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public MedicationPlanDTO findMedicationPlanById(UUID medicationPlanId) {
        Optional<MedicationPlan> prosumerOptionalMedicationPlan = medicationPlanRepository.findById(medicationPlanId);
        if (!prosumerOptionalMedicationPlan.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", medicationPlanId);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationPlanId);
        }
        return MedicationPlanBuilder.toMedicationPlanDTO(prosumerOptionalMedicationPlan.get());
    }

    public UUID insertMedicationPlan(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        LOGGER.debug("Medication plan with id {} was inserted in db", medicationPlan.getMedicationPlanId());
        return medicationPlan.getMedicationPlanId();
    }

    public MedicationPlanDTO deleteMedicationPlanById(UUID medicationPlanId){
        Optional<MedicationPlan> prosumerOptionalMedicationPlan = medicationPlanRepository.findById(medicationPlanId);
        if (!prosumerOptionalMedicationPlan.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", medicationPlanId);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationPlanId);
        }

        medicationPlanRepository.deleteById(medicationPlanId);
        return MedicationPlanBuilder.toMedicationPlanDTO(prosumerOptionalMedicationPlan.get());
    }

    public void addMedicationToMedicationPlan(String medicationName, UUID medicationPlanId, String dailyInterval) {
        Medication medication = medicationRepository.findMedicationByName(medicationName);
        Optional<MedicationPlan> prosumerOptionalMedicationPlan = medicationPlanRepository.findById(medicationPlanId);

        if (!prosumerOptionalMedicationPlan.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", medicationPlanId);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + medicationPlanId);
        }

        MedicationPlanToMedication medicationPlanToMedication = new MedicationPlanToMedication(dailyInterval);
        medicationPlanToMedication.setMedication(medication);
        medicationPlanToMedication.setMedicationPlan(prosumerOptionalMedicationPlan.get());

        medicationPlanToMedicationRepository.save(medicationPlanToMedication);
        prosumerOptionalMedicationPlan.get().addMedicationPlanToMedication(medicationPlanToMedication);

        medicationPlanRepository.save(prosumerOptionalMedicationPlan.get());
    }

}
