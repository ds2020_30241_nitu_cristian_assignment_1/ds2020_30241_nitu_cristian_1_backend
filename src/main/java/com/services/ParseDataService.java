package com.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ParseDataService {

    public long getTime(String dateAsString1, String dateAsString2) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date1 = formatter.parse(dateAsString1);
        Date date2 = formatter.parse(dateAsString2);

        long result = 0;

        long hour = date2.getHours() * 3600 - date1.getHours() * 3600;
        int minutes = date2.getMinutes() * 60 - date1.getMinutes() * 60;
        int seconds = date2.getSeconds() - date1.getSeconds();


        if(date2.getHours() < date1.getHours() ||
                date2.getHours() == date1.getHours() && date2.getMinutes() < date1.getMinutes() ||
                date2.getHours() == date1.getHours() && date2.getMinutes() == date1.getMinutes() && date2.getSeconds() < date1.getSeconds())
        {
            result = 3600 * 24 + hour + minutes + seconds;
        }

        else result = hour + minutes + seconds;

        return result;
    }
}
