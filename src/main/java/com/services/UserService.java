package com.services;

import com.builders.UserBuilder;
import com.commons.dtos.UserDTO;
import com.entities.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDTO> findUsers() {
        List<Users> userList = userRepository.findAll();
        return userList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public UserDTO findUserById(UUID userId) {
        Optional<Users> prosumerOptionalUser = userRepository.findById(userId);
        if (!prosumerOptionalUser.isPresent()) {
            LOGGER.error("User with id {} was not found in db", userId);
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + userId);
        }
        return UserBuilder.toUserDTO(prosumerOptionalUser.get());
    }

    public UUID insertUser(UserDTO userDTO) {
        Users user = UserBuilder.toEntity(userDTO);
        user = userRepository.save(user);
        LOGGER.debug("User with id {} was inserted in db", user.getUserId());
        return user.getUserId();
    }

    public UserDTO deleteUserById(UUID userId){
        Optional<Users> prosumerOptionalUser = userRepository.findById(userId);

        if (!prosumerOptionalUser.isPresent()) {
            LOGGER.error("User with id {} was not found in db", userId);
            throw new ResourceNotFoundException(Users.class.getSimpleName() + " with id: " + userId);
        }

        userRepository.deleteById(userId);
        return UserBuilder.toUserDTO(prosumerOptionalUser.get());
    }

    public UserDTO login(String username, String password) {
        List<Users> userList = userRepository.findAll();

        for(Users u: userList){
            if(u.getUsername().equals(username) && u.getPassword().equals(password)){
                return UserBuilder.toUserDTO(u);
            }
        }

        return null;
    }

}
