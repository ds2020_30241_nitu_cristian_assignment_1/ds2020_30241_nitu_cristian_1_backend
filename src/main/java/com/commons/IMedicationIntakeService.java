package com.commons;

import com.commons.dtos.MedicationIntakeDTO;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public interface IMedicationIntakeService {

    public void takeMedication(MedicationIntakeDTO medicationIntakeDTO, UUID patientId);
}
