package com.commons;

import com.commons.dtos.MedicationIntakeDTO;
import com.commons.dtos.MedicationPlanDTO;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;

@Service
public interface IPatientService {

    Set<MedicationPlanDTO> findPlansByPatientId(UUID id);
    public void takeMedication(MedicationIntakeDTO medicationIntakeDTO, UUID patientId);
}
