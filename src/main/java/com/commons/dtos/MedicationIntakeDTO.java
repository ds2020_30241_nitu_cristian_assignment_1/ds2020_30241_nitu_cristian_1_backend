package com.commons.dtos;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class MedicationIntakeDTO implements Serializable {
    private UUID medicationIntakeId;
    @NotNull
    private String medication;
    @NotNull
    private String date;
    @NotNull
    private String dailyInterval;
    @NotNull
    private boolean taken;

    public MedicationIntakeDTO() {
    }

    public MedicationIntakeDTO(UUID medicationIntakeId, String medication, String date, String dailyInterval, boolean taken) {
        this.medicationIntakeId = medicationIntakeId;
        this.medication = medication;
        this.date = date;
        this.dailyInterval = dailyInterval;
        this.taken = taken;
    }

    public MedicationIntakeDTO(String medication, String date, String dailyInterval, boolean taken) {
        this.medication = medication;
        this.date = date;
        this.dailyInterval = dailyInterval;
        this.taken = taken;
    }

    public UUID getMedicationIntakeId() {
        return medicationIntakeId;
    }

    public void setMedicationIntakeId(UUID medicationIntakeId) {
        this.medicationIntakeId = medicationIntakeId;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public boolean getTaken() { return taken; }

    public void setTaken(boolean taken) { this.taken = taken; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationIntakeDTO medicationIntakeDTO = (MedicationIntakeDTO) o;
        return Objects.equals(medication, medicationIntakeDTO.medication) &&
                Objects.equals(date, medicationIntakeDTO.date) &&
                Objects.equals(dailyInterval, medicationIntakeDTO.dailyInterval) &&
                Objects.equals(taken, medicationIntakeDTO.taken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medication, date, dailyInterval, taken);
    }
}
