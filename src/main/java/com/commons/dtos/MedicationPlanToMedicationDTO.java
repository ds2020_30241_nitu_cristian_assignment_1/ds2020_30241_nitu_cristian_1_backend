package com.commons.dtos;

import java.io.Serializable;
import java.util.Objects;

public class MedicationPlanToMedicationDTO implements Serializable {

    private String dailyInterval;

    private String medicationName;

    public MedicationPlanToMedicationDTO() {
    }

    public MedicationPlanToMedicationDTO(String dailyInterval, String medicationName) {
        this.dailyInterval = dailyInterval;
        this.medicationName = medicationName;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedication(String medicationName) {
        this.medicationName = medicationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanToMedicationDTO medicationPlanToMedicationDTO = (MedicationPlanToMedicationDTO) o;
        return Objects.equals(dailyInterval, medicationPlanToMedicationDTO.dailyInterval);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dailyInterval);
    }
}