package com.commons.dtos;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class MedicationDTO implements Serializable {
    private UUID medicationId;
    @NotNull
    private String name;

    private int dosage;
    @NotNull
    private String sideEffect;

    public MedicationDTO() {
    }

    public MedicationDTO(UUID medicationId, String name, int dosage, String sideEffect) {
        this.medicationId = medicationId;
        this.name = name;
        this.dosage = dosage;
        this.sideEffect = sideEffect;
    }

    public MedicationDTO(String name, int dosage, String sideEffect) {
        this.name = name;
        this.dosage = dosage;
        this.sideEffect = sideEffect;
    }


    public UUID getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(UUID medicationId) {
        this.medicationId = medicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO medicationDTO = (MedicationDTO) o;
        return Objects.equals(name, medicationDTO.name) &&
                dosage == medicationDTO.dosage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dosage);
    }
}
