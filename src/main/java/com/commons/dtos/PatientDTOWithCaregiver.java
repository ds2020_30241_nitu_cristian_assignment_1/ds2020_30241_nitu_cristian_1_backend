package com.commons.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class PatientDTOWithCaregiver extends RepresentationModel<PatientDTOWithCaregiver> {
    private UUID patientId;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String role;
    @NotNull
    private String name;
    @NotNull
    private String birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    @NotNull
    private String medicalRecord;

    private Set<MedicationPlanDTO> medicationPlanDTOSet = new HashSet<>();
    private CaregiverDTOName caregiverDTOName;

    public PatientDTOWithCaregiver() {
    }

    public PatientDTOWithCaregiver(UUID patientId, String username, String password, String role, String name, String birthdate,
                      String gender, String address, String medicalRecord) {
        this.patientId = patientId;
        this.username = username;
        this.password = password;
        this.role = role;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Set<MedicationPlanDTO> getMedicationPlanDTOSet() { return medicationPlanDTOSet; }

    public void setMedicationPlanDTOSet(Set<MedicationPlanDTO> medicationPlanDTOSet) {
        this.medicationPlanDTOSet = medicationPlanDTOSet;
    }

    public CaregiverDTOName getCaregiverDTOName() { return caregiverDTOName; }

    public void setCaregiverDTOName(CaregiverDTOName caregiverDTOName) { this.caregiverDTOName = caregiverDTOName; }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTOWithCaregiver patientDTO = (PatientDTOWithCaregiver) o;
        return Objects.equals(username, patientDTO.username) &&
                Objects.equals(password, patientDTO.password) &&
                Objects.equals(role, patientDTO.role) &&
                Objects.equals(name, patientDTO.name) &&
                Objects.equals(birthdate, patientDTO.birthdate) &&
                Objects.equals(gender, patientDTO.gender) &&
                Objects.equals(address, patientDTO.address) &&
                Objects.equals(medicalRecord, patientDTO.medicalRecord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, role, name, birthdate, gender, address, medicalRecord);
    }
}
