package com.commons.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class CaregiverDTO extends RepresentationModel<CaregiverDTO> {
    private UUID caregiverId;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String role;
    @NotNull
    private String name;
    @NotNull
    private String birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;

    private Set<PatientDTO> patientDTOSet = new HashSet<>();

    public CaregiverDTO() {
    }

    public CaregiverDTO(UUID caregiverId, String username, String password, String role, String name,
                        String birthdate, String gender, String address) {
        this.caregiverId = caregiverId;
        this.username = username;
        this.password = password;
        this.role = role;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }

    public UUID getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(UUID caregiverId) {
        this.caregiverId = caregiverId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<PatientDTO> getPatientDTOSet() { return patientDTOSet; }

    public void setPatientDTOSet(Set<PatientDTO> patientDTOSet) { this.patientDTOSet = patientDTOSet; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return Objects.equals(username, caregiverDTO.username) &&
                Objects.equals(password, caregiverDTO.password) &&
                Objects.equals(role, caregiverDTO.role) &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(birthdate, caregiverDTO.birthdate) &&
                Objects.equals(gender, caregiverDTO.gender) &&
                Objects.equals(address, caregiverDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, role, name, birthdate, gender, address);
    }
}
