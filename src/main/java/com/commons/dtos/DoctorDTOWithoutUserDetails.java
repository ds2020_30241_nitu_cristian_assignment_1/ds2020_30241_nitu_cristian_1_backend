package com.commons.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class DoctorDTOWithoutUserDetails extends RepresentationModel<DoctorDTOWithoutUserDetails> {
    private UUID doctorId;
    @NotNull
    private String name;
    @NotNull
    private String birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;

    public DoctorDTOWithoutUserDetails() {
    }

    public DoctorDTOWithoutUserDetails(UUID doctorId, String name, String birthdate, String gender, String address) {
        this.doctorId = doctorId;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }

    public UUID getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(UUID doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDTOWithoutUserDetails doctorDTO = (DoctorDTOWithoutUserDetails) o;
        return  Objects.equals(name, doctorDTO.name) &&
                Objects.equals(birthdate, doctorDTO.birthdate) &&
                Objects.equals(gender, doctorDTO.gender) &&
                Objects.equals(address, doctorDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birthdate, gender, address);
    }
}
