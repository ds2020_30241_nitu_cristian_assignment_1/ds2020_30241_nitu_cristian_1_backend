package com.commons.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class DoctorDTO extends RepresentationModel<DoctorDTO> {
    private UUID doctorId;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String role;
    @NotNull
    private String name;
    @NotNull
    private String birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;

    public DoctorDTO() {
    }

    public DoctorDTO(UUID doctorId, String username, String password, String role, String name, String birthdate,
                     String gender, String address) {
        this.doctorId = doctorId;
        this.username = username;
        this.password = password;
        this.role = role;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }

    public UUID getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(UUID doctorId) {
        this.doctorId = doctorId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return Objects.equals(username, doctorDTO.username) &&
                Objects.equals(password, doctorDTO.password) &&
                Objects.equals(role, doctorDTO.role) &&
                Objects.equals(name, doctorDTO.name) &&
                Objects.equals(birthdate, doctorDTO.birthdate) &&
                Objects.equals(gender, doctorDTO.gender) &&
                Objects.equals(address, doctorDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, role, name, birthdate, gender, address);
    }
}
