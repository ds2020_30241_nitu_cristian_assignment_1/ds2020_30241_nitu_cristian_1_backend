package com.commons.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class CaregiverDTOName extends RepresentationModel<CaregiverDTOName> {
    @NotNull
    private String role;
    @NotNull
    private String name;
    @NotNull
    private String gender;

    public CaregiverDTOName() {
    }

    public CaregiverDTOName(String role, String name, String gender) {
        this.role = role;
        this.name = name;
        this.gender = gender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTOName caregiverDTO = (CaregiverDTOName) o;
        return  Objects.equals(role, caregiverDTO.role) &&
                Objects.equals(name, caregiverDTO.name) &&
                 Objects.equals(gender, caregiverDTO.gender);

    }

    @Override
    public int hashCode() {
        return Objects.hash(role, name, gender);
    }
}
