package com.commons.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class PatientActivityDTO extends RepresentationModel<PatientActivityDTO> {
    private UUID patientActivityId;
    @NotNull
    private String startActivity;
    @NotNull
    private String endActivity;
    @NotNull
    private String activity;

    public PatientActivityDTO() {

    }

    public PatientActivityDTO(UUID patientActivityId, String startActivity, String endActivity, String activity) {
        this.patientActivityId = patientActivityId;
        this.startActivity = startActivity;
        this.endActivity = endActivity;
        this.activity = activity;
    }

    public UUID getPatientActivityId() {
        return patientActivityId;
    }

    public void setPatientActivityId(UUID patientActivityId) {
        this.patientActivityId = patientActivityId;
    }

    public String getStartActivity() {
        return startActivity;
    }

    public void setStartActivity(String startActivity) {
        this.startActivity = startActivity;
    }

    public String getEndActivity() {
        return endActivity;
    }

    public void setEndActivity(String endActivity) {
        this.endActivity = endActivity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientActivityDTO patientActivityDTO = (PatientActivityDTO) o;
        return Objects.equals(startActivity, patientActivityDTO.startActivity) &&
                Objects.equals(endActivity, patientActivityDTO.endActivity) &&
                Objects.equals(activity, patientActivityDTO.activity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startActivity, endActivity, activity);
    }
}
