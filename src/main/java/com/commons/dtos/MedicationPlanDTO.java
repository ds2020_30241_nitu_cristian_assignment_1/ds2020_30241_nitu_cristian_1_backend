package com.commons.dtos;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class MedicationPlanDTO implements Serializable {
    private UUID medicationPlanId;

    private String startTreatment;

    private String endTreatment;

    private Set<MedicationPlanToMedicationDTO> medicationPlanToMedicationDTOSet = new HashSet<>();

    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(UUID medicationPlanId, String startTreatment, String endTreatment) {
        this.medicationPlanId = medicationPlanId;
        this.startTreatment = startTreatment;
        this.endTreatment = endTreatment;
    }

    public MedicationPlanDTO(String startTreatment, String endTreatment) {
        this.startTreatment = startTreatment;
        this.endTreatment = endTreatment;
    }

    public UUID getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(UUID medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getStartTreatment() {
        return startTreatment;
    }

    public void setStartTreatment(String startTreatment) {
        this.startTreatment = startTreatment;
    }

    public String getEndTreatment() {
        return endTreatment;
    }

    public void setEndTreatment(String endTreatment) {
        this.endTreatment = endTreatment;
    }

    public Set<MedicationPlanToMedicationDTO> getMedicationPlanToMedicationDTOSet() {
        return medicationPlanToMedicationDTOSet;
    }

    public void setMedicationPlanToMedicationDTOSet(Set<MedicationPlanToMedicationDTO> medicationPlanToMedicationDTOSet) {
        this.medicationPlanToMedicationDTOSet = medicationPlanToMedicationDTOSet;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDTO medicationPlanDTO = (MedicationPlanDTO) o;
        return Objects.equals(startTreatment, medicationPlanDTO.startTreatment) &&
                Objects.equals(endTreatment, medicationPlanDTO.endTreatment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTreatment, endTreatment);
    }
}