package com.repositories;

import com.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {

    Caregiver findCaregiverByName(String name);
}
