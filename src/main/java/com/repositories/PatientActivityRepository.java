package com.repositories;

import com.entities.PatientActivity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PatientActivityRepository extends JpaRepository<PatientActivity, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    PatientActivity findPatientActivitiesByActivity(String activity);

}
