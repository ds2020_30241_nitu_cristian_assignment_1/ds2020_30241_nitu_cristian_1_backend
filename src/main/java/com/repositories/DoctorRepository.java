package com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.entities.Doctor;

import java.util.List;
import java.util.UUID;

public interface DoctorRepository extends JpaRepository<Doctor, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Doctor> findDoctorByName(String name);

}
