package com.repositories;

import com.entities.MedicationPlanToMedication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MedicationPlanToMedicationRepository extends JpaRepository<MedicationPlanToMedication, UUID> {

}
