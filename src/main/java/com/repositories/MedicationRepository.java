package com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.entities.Medication;

import java.util.UUID;

public interface MedicationRepository extends JpaRepository<Medication, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    Medication findMedicationByName(String name);

}
