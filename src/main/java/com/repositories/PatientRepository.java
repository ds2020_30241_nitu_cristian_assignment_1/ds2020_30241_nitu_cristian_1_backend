package com.repositories;

import com.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface PatientRepository extends JpaRepository<Patient, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
   // Patient findPatientByName(String name);

    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "WHERE p.name = :name ")
    Patient findPatientByName(@Param("name") String name);
}
