package com.repositories;

import com.entities.MedicationIntake;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MedicationIntakeRepository extends JpaRepository<MedicationIntake, UUID> {

}
