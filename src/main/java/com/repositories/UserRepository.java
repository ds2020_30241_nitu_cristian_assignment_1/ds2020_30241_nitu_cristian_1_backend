package com.repositories;

import com.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<Users, UUID> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Users> findUserByUsername(String username);

}
