package com.entities;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class MedicationPlanToMedicationKey implements Serializable {

    @Column(name = "medication_plan_id")
    @Type(type = "uuid-binary")
    private UUID medicationPlanId;

    @Column(name = "medication_id")
    @Type(type = "uuid-binary")
    private UUID medicationId;

    public UUID getMedicationPlanId() {
        return medicationPlanId;
    }

    public UUID getMedicationId() {
        return medicationId;
    }

}