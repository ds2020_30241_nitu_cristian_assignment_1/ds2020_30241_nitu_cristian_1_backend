package com.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
public class Medication {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID medicationId;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "dosage", nullable = false)
    private int dosage;

    @Column(name = "sideEffect", nullable = false)
    private String sideEffect;

    @OneToMany(mappedBy = "medication")
    private Set<MedicationPlanToMedication> medicationPlanToMedicationSet = new HashSet<>();

    public Medication() {
    }

    public Medication(String name, int dosage, String sideEffect) {
        this.name = name;
        this.dosage = dosage;
        this.sideEffect = sideEffect;
    }

    public UUID getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(UUID medicationId) {
        this.medicationId = medicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public Set<MedicationPlanToMedication> medicationPlanToMedications() {
        return medicationPlanToMedicationSet;
    }

    public void setMedicationPlanToMedicationSet(Set<MedicationPlanToMedication> medicationPlanToMedicationSet) {
        this.medicationPlanToMedicationSet = medicationPlanToMedicationSet;
    }
}
