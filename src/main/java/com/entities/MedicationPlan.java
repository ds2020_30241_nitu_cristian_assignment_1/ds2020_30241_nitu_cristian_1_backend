package com.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
public class MedicationPlan  {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID medicationPlanId;

    @Column(name = "startTreatment", nullable = false)
    private String startTreatment;

    @Column(name = "endTreatment", nullable = false)
    private String endTreatment;

    @ManyToOne(fetch = FetchType.EAGER)
    private Patient patient;

    @OneToMany(mappedBy = "medicationPlan", fetch = FetchType.EAGER)
    private Set<MedicationPlanToMedication> medicationPlanToMedicationSet = new HashSet<>();

    public MedicationPlan() {
    }

    public MedicationPlan(String startTreatment, String endTreatment) {
        this.startTreatment = startTreatment;
        this.endTreatment = endTreatment;
    }

    public void addMedicationPlanToMedication(MedicationPlanToMedication medicationPlanToMedication) {
        medicationPlanToMedicationSet.add(medicationPlanToMedication);
        medicationPlanToMedication.setMedicationPlan(this);
    }

    public UUID getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(UUID medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getStartTreatment() {
        return startTreatment;
    }

    public void setStartTreatment(String startTreatment) {
        this.startTreatment = startTreatment;
    }

    public String getEndTreatment() {
        return endTreatment;
    }

    public void setEndTreatment(String endTreatment) {
        this.endTreatment = endTreatment;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Set<MedicationPlanToMedication> getMedicationPlanToMedicationSet() {
        return medicationPlanToMedicationSet;
    }

    public void setMedicationPlanToMedicationSet(Set<MedicationPlanToMedication> medicationPlanToMedicationSet) {
        this.medicationPlanToMedicationSet = medicationPlanToMedicationSet;
    }

}
