package com.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
public class PatientActivity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID activityId;

    @Column(name = "start_activity", nullable = false)
    private String startActivity;

    @Column(name = "end_activity", nullable = false)
    private String endActivity;

    @Column(name = "activity", nullable = false)
    private String activity;

    @ManyToOne(fetch = FetchType.EAGER)
    private Patient activityToPatient;


    public PatientActivity() {
    }

    public PatientActivity(String startActivity, String endActivity, String activity) {
        this.startActivity = startActivity;
        this.endActivity = endActivity;
        this.activity = activity;
    }

    public UUID getActivityId() {
        return activityId;
    }

    public void setActivityId(UUID activityId) {
        this.activityId = activityId;
    }

    public String getStart() {
        return startActivity;
    }

    public void setStart(String startActivity) {
        this.startActivity = startActivity;
    }

    public String getEnd() {
        return endActivity;
    }

    public void setEnd(String endActivity) {
        this.endActivity = endActivity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Patient getPatient() {
        return activityToPatient;
    }

    public void setPatient(Patient patient) {
        this.activityToPatient = patient;
    }


}
