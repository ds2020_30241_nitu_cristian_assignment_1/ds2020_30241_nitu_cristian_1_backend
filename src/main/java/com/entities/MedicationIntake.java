package com.entities;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class MedicationIntake {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID medicationIntakeId;

    @Column(name = "medication", nullable = false)
    private String medication;

    @Column(name = "date", nullable = false)
    private String date;

    @Column(name = "dailyInterval", nullable = false)
    private String dailyInterval;

    @Column(name = "taken", nullable = false)
    private boolean taken;

    @ManyToOne(fetch = FetchType.EAGER)
    private Patient patient;


    public MedicationIntake() {
    }

    public MedicationIntake(String medication, String date, String dailyInterval, boolean taken) {
        this.medication = medication;
        this.date = date;
        this.dailyInterval = dailyInterval;
        this.taken = taken;
    }

    public UUID getMedicationIntakeId() {
        return medicationIntakeId;
    }

    public void setMedicationIntakeId(UUID medicationIntakeId) {
        this.medicationIntakeId = medicationIntakeId;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDailyInterval() { return dailyInterval; }

    public void setDailyInterval(String dailyInterval) { this.dailyInterval = dailyInterval; }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public boolean getTaken() { return taken; }

    public void setTaken(boolean taken) { this.taken = taken; }
}
