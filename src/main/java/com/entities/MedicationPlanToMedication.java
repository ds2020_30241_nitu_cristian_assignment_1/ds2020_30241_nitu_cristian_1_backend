package com.entities;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
public class MedicationPlanToMedication  {

    @EmbeddedId
    MedicationPlanToMedicationKey id = new MedicationPlanToMedicationKey();

    @ManyToOne
    @MapsId("medicationPlanId")
    @JoinColumn(name = "medication_plan_id")
    private MedicationPlan medicationPlan;

    @ManyToOne
    @MapsId("medicationId")
    @JoinColumn(name = "medication_id")
    private Medication medication;

    private String dailyInterval;

    public MedicationPlanToMedication() {

    }

    public MedicationPlanToMedication(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

}