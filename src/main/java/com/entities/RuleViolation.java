package com.entities;

public class RuleViolation {
    private String startActivity;
    private String endActivity;
    private String activity;
    private String patientId;
    private String caregiverId;
    private String patientUsername;
    private String msg;

    public RuleViolation() {
    }

    public RuleViolation(String startActivity, String endActivity, String activity,
                    String patientId, String caregiverId, String patientUsername, String msg) {

        this.startActivity = startActivity;
        this.endActivity = endActivity;
        this.activity = activity;
        this.patientId = patientId;
        this.caregiverId = caregiverId;
        this.patientUsername = patientUsername;
        this.msg = msg;
    }

    public String getStartActivity() {
        return startActivity;
    }

    public String getEndActivity() {
        return endActivity;
    }

    public String getActivity() {
        return activity;
    }

    public String getPatientId() {
        return patientId;
    }

    public String getCaregiverId() {
        return caregiverId;
    }

    public String getPatientUsername() {
        return patientUsername;
    }

    public String getMsg() {
        return msg;
    }
}
