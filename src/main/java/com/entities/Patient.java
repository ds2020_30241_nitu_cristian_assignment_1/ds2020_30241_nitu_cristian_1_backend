package com.entities;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Patient extends Users {
    private static final long serialVersionUID = 1L;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "birthdate", nullable = false)
    private String birthdate;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "medical_record", nullable = false)
    private String medicalRecord;

    @ManyToOne(fetch = FetchType.EAGER)
    private Caregiver caregiver;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    Set<MedicationPlan> medicationPlans = new HashSet<>();

    @OneToMany(mappedBy = "activityToPatient", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    Set<PatientActivity> patientActivities = new HashSet<>();

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    Set<MedicationIntake> medicationsIntake = new HashSet<>();


    public Patient() {

    }

    public Patient(String username, String password, String role, String name, String birthdate,
                   String gender, String address, String medicalRecord) {
        super(username, password, role);
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public void addMedicationPlan(MedicationPlan medicationPlan) {
        medicationPlans.add(medicationPlan);
        medicationPlan.setPatient(this);
    }

    public void removeMedicationPlan(MedicationPlan medicationPlan) {
        medicationPlans.remove(medicationPlan);
        medicationPlan.setPatient(null);
    }

    public void addPatientActivity(PatientActivity patientActivity) {
        patientActivities.add(patientActivity);
        patientActivity.setPatient(this);
    }

    public void removePatientActivity(PatientActivity patientActivity) {
        patientActivities.remove(patientActivity);
        patientActivity.setPatient(null);
    }

    public void addMedicationIntake(MedicationIntake medicationIntake) {
        medicationsIntake.add(medicationIntake);
        medicationIntake.setPatient(this);
    }

    public void removeMedicationIntake(MedicationIntake medicationIntake) {
        medicationsIntake.remove(medicationIntake);
        medicationIntake.setPatient(null);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public Set<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public Set<PatientActivity> getPatientActivities() {
        return patientActivities;
    }

    public void setPatientActivities(Set<PatientActivity> patientActivities) {
        this.patientActivities = patientActivities;
    }

    public Set<MedicationIntake> getMedicationsIntake() {
        return medicationsIntake;
    }

    public void setMedicationsIntake(Set<MedicationIntake> medicationsIntake) {
        this.medicationsIntake = medicationsIntake;
    }

}
