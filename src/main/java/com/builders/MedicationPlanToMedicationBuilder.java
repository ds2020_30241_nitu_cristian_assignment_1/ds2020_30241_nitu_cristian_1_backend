package com.builders;

import com.commons.dtos.MedicationPlanToMedicationDTO;
import com.entities.MedicationPlanToMedication;

public class MedicationPlanToMedicationBuilder {

    private MedicationPlanToMedicationBuilder() {
    }

    public static MedicationPlanToMedicationDTO toMedicationLinkDTO(MedicationPlanToMedication medicationPlanToMedication) {
        return new MedicationPlanToMedicationDTO(medicationPlanToMedication.getDailyInterval(),
                medicationPlanToMedication.getMedication().getName());

    }

    public static MedicationPlanToMedication toEntity(MedicationPlanToMedicationDTO medicationPlanToMedicationDTO) {
        MedicationPlanToMedication medicationPlanToMedication =
                new MedicationPlanToMedication(medicationPlanToMedicationDTO.getDailyInterval());
        medicationPlanToMedication.setMedication(medicationPlanToMedication.getMedication());

        return medicationPlanToMedication;
    }
}
