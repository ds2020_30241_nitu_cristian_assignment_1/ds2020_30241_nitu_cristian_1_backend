package com.builders;

import com.commons.dtos.PatientActivityDTO;
import com.entities.PatientActivity;


public class PatientActivityBuilder {

    private PatientActivityBuilder() {
    }

    public static PatientActivityDTO toPatientActivityDTO(PatientActivity patientActivity) {
        PatientActivityDTO patientActivityDTO = new PatientActivityDTO(patientActivity.getActivityId(),
            patientActivity.getStart(), patientActivity.getEnd(), patientActivity.getActivity());

        return patientActivityDTO;
    }

    public static PatientActivity toEntity(PatientActivityDTO patientActivityDTO) {
        PatientActivity patientActivity = new PatientActivity(patientActivityDTO.getStartActivity(),
                patientActivityDTO.getEndActivity(), patientActivityDTO.getActivity());

        return patientActivity;
    }
}
