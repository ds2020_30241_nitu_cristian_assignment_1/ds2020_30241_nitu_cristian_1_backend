package com.builders;

import com.commons.dtos.MedicationDTO;
import com.entities.Medication;


public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO toMedicationDTO(Medication medication) {
        MedicationDTO medicationDTO = new MedicationDTO(medication.getMedicationId(),
                medication.getName(), medication.getDosage(), medication.getSideEffect());

        return medicationDTO;
    }

    public static Medication toEntity(MedicationDTO medicationDTO) {
        Medication medication = new Medication(medicationDTO.getName(), medicationDTO.getDosage(), medicationDTO.getSideEffect());

        return medication;
    }
}
