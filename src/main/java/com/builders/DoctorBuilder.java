package com.builders;

import com.commons.dtos.DoctorDTO;
import com.commons.dtos.DoctorDTOWithoutUserDetails;
import com.entities.Doctor;

public class DoctorBuilder {

    private DoctorBuilder() {
    }

    public static DoctorDTO toDoctorDTO(Doctor doctor) {
        return new DoctorDTO(doctor.getUserId(), doctor.getUsername(), doctor.getPassword(), doctor.getRole(),
                doctor.getName(), doctor.getBirthdate(), doctor.getGender(), doctor.getAddress());
    }

    public static DoctorDTOWithoutUserDetails toDoctorDTOWithoutDetails(Doctor doctor) {
        return new DoctorDTOWithoutUserDetails(doctor.getUserId(), doctor.getName(),
                doctor.getBirthdate(), doctor.getGender(), doctor.getAddress());
    }


    public static Doctor toEntity(DoctorDTO doctorDTO) {
        return new Doctor(doctorDTO.getUsername(), doctorDTO.getPassword(), doctorDTO.getRole(),
                doctorDTO.getName(), doctorDTO.getBirthdate(), doctorDTO.getGender(), doctorDTO.getAddress());
    }
}
