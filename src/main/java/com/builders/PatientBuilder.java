package com.builders;

import com.commons.dtos.*;
import com.entities.MedicationIntake;
import com.entities.MedicationPlan;
import com.entities.Patient;
import com.entities.PatientActivity;
import java.util.HashSet;
import java.util.Set;

public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {
        PatientDTO patientDTO = new PatientDTO(patient.getUserId(), patient.getUsername(), patient.getPassword(),
                patient.getRole(), patient.getName(), patient.getBirthdate(), patient.getGender(),
                patient.getAddress(), patient.getMedicalRecord());

        Set<MedicationPlanDTO> medicationPlanDTOSet = new HashSet<>();

        for (MedicationPlan medicationPlan: patient.getMedicationPlans()) {
            MedicationPlanDTO medicationPlanDTO = MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan);
            medicationPlanDTOSet.add(medicationPlanDTO);
        }

        patientDTO.setMedicationPlanDTOSet(medicationPlanDTOSet);


        Set<PatientActivityDTO> patientActivityDTOSet = new HashSet<>();

        for (PatientActivity patientActivity: patient.getPatientActivities()) {
            PatientActivityDTO patientActivityDTO = PatientActivityBuilder.toPatientActivityDTO(patientActivity);
            patientActivityDTOSet.add(patientActivityDTO);
        }

        patientDTO.setPatientActivityDTOSet(patientActivityDTOSet);

        Set<MedicationIntakeDTO> medicationIntakeDTOSet = new HashSet<>();

        for(MedicationIntake medicationIntake: patient.getMedicationsIntake()) {
            MedicationIntakeDTO medicationIntakeDTO = MedicationIntakeBuilder.toMedicationIntakeDTO(medicationIntake);
            medicationIntakeDTOSet.add(medicationIntakeDTO);
        }

        patientDTO.setMedicationIntakeDTOSet(medicationIntakeDTOSet);

        return patientDTO;
    }

    public static PatientDTOWithCaregiver toPatientDTOWithCaregiver(Patient patient) {
        PatientDTOWithCaregiver patientDTOWithCaregiver = new PatientDTOWithCaregiver(patient.getUserId(),
                patient.getUsername(), patient.getPassword(), patient.getRole(), patient.getName(),
                patient.getBirthdate(), patient.getGender(), patient.getAddress(), patient.getMedicalRecord());

        Set<MedicationPlanDTO> medicationPlanDTOSet = new HashSet<>();

        for (MedicationPlan medicationPlan: patient.getMedicationPlans()) {
            MedicationPlanDTO medicationPlanDTO = MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan);
            medicationPlanDTOSet.add(medicationPlanDTO);
        }

        patientDTOWithCaregiver.setMedicationPlanDTOSet(medicationPlanDTOSet);
        patientDTOWithCaregiver.setCaregiverDTOName(CaregiverBuilder.toCaregiverDTOName(patient.getCaregiver()));
        return patientDTOWithCaregiver;
    }

    public static Patient toEntity(PatientDTO patientDTO) {
        Patient patient = new Patient(patientDTO.getUsername(), patientDTO.getPassword(), patientDTO.getRole(),
                patientDTO.getName(), patientDTO.getBirthdate(), patientDTO.getGender(),
                patientDTO.getAddress(), patientDTO.getMedicalRecord());

        Set<MedicationPlan> medicationPlanSet = new HashSet<MedicationPlan>();

        for (MedicationPlanDTO medicationPlanDTO : patientDTO.getMedicationPlanDTOSet()) {
            MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
            medicationPlan.setPatient(patient);
            medicationPlanSet.add(medicationPlan);
        }

        patient.setMedicationPlans(medicationPlanSet);

        Set<PatientActivity> patientActivitySet = new HashSet<PatientActivity>();

        for (PatientActivityDTO patientActivityDTO : patientDTO.getPatientActivityDTOSet()) {
            PatientActivity patientActivity = PatientActivityBuilder.toEntity(patientActivityDTO);
            patientActivity.setPatient(patient);
            patientActivitySet.add(patientActivity);
        }

        patient.setPatientActivities(patientActivitySet);

        Set<MedicationIntake> medicationIntakeSet = new HashSet<>();

        for(MedicationIntakeDTO medicationIntakeDTO: patientDTO.getMedicationIntakeDTOSet()) {
            MedicationIntake medicationIntake = MedicationIntakeBuilder.toEntity(medicationIntakeDTO);
            medicationIntake.setPatient(patient);
            medicationIntakeSet.add(medicationIntake);
        }

        patient.setMedicationsIntake(medicationIntakeSet);

        return patient;
    }
}
