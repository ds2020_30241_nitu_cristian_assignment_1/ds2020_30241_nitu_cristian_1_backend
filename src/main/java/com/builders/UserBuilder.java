package com.builders;

import com.commons.dtos.UserDTO;
import com.entities.Users;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO toUserDTO(Users user) {
        return new UserDTO(user.getUserId(), user.getUsername(), user.getPassword(), user.getRole());
    }

    public static Users toEntity(UserDTO userDTO) {
        return new Users(userDTO.getUsername(), userDTO.getPassword(), userDTO.getRole());
    }
}
