package com.builders;

import com.commons.dtos.MedicationPlanDTO;
import com.commons.dtos.MedicationPlanToMedicationDTO;
import com.entities.MedicationPlan;
import com.entities.MedicationPlanToMedication;

import java.util.HashSet;
import java.util.Set;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        MedicationPlanDTO medicationPlanDTO = new MedicationPlanDTO(medicationPlan.getMedicationPlanId(),
                medicationPlan.getStartTreatment(), medicationPlan.getEndTreatment());

        Set<MedicationPlanToMedicationDTO> medicationPlanToMedicationDTOSet = new HashSet<>();

        for (MedicationPlanToMedication medicationPlanToMedication: medicationPlan.getMedicationPlanToMedicationSet()) {
            MedicationPlanToMedicationDTO medicationPlanToMedicationDTO =
                    MedicationPlanToMedicationBuilder.toMedicationLinkDTO(medicationPlanToMedication);
            medicationPlanToMedicationDTOSet.add(medicationPlanToMedicationDTO);
        }

        medicationPlanDTO.setMedicationPlanToMedicationDTOSet(medicationPlanToMedicationDTOSet);

        return medicationPlanDTO;
    }

    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medicationPlan = new MedicationPlan(medicationPlanDTO.getStartTreatment(),
                medicationPlanDTO.getEndTreatment());

        Set<MedicationPlanToMedication> medicationPlanToMedicationsSet = new HashSet<>();

        for (MedicationPlanToMedicationDTO medicationPlanToMedicationDTO : medicationPlanDTO.getMedicationPlanToMedicationDTOSet()) {
            MedicationPlanToMedication medicationPlanToMedication =
                    MedicationPlanToMedicationBuilder.toEntity(medicationPlanToMedicationDTO);
            medicationPlanToMedicationsSet.add(medicationPlanToMedication);
        }

        medicationPlan.setMedicationPlanToMedicationSet(medicationPlanToMedicationsSet);

        return medicationPlan;
    }
}
