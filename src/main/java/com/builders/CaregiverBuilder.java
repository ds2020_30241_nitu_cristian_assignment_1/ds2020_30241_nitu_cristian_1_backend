package com.builders;

import com.commons.dtos.CaregiverDTO;
import com.commons.dtos.CaregiverDTOName;
import com.commons.dtos.PatientDTO;
import com.entities.Caregiver;
import com.entities.Patient;

import java.util.HashSet;
import java.util.Set;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        CaregiverDTO caregiverDTO = new CaregiverDTO(caregiver.getUserId(), caregiver.getUsername(), caregiver.getPassword(), caregiver.getRole(),
                caregiver.getName(), caregiver.getBirthdate(), caregiver.getGender(), caregiver.getAddress());

        Set<PatientDTO> patientDTOSet = new HashSet<>();

        for (Patient patient : caregiver.getPatients()) {
            PatientDTO patientDTO = PatientBuilder.toPatientDTO(patient);
            patientDTOSet.add(patientDTO);
        }

        caregiverDTO.setPatientDTOSet(patientDTOSet);

        return caregiverDTO;
    }

    public static CaregiverDTOName toCaregiverDTOName(Caregiver caregiver) {
        CaregiverDTOName caregiverDTOName = new CaregiverDTOName(caregiver.getRole(),
                caregiver.getName(), caregiver.getGender());

        return caregiverDTOName;
    }

    public static Caregiver toEntity(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = new Caregiver(caregiverDTO.getUsername(), caregiverDTO.getPassword(), caregiverDTO.getRole(),
                caregiverDTO.getName(), caregiverDTO.getBirthdate(), caregiverDTO.getGender(), caregiverDTO.getAddress());

        Set<Patient> patients = new HashSet<Patient>();

        for (PatientDTO patientDTO : caregiverDTO.getPatientDTOSet()) {
            Patient patient = PatientBuilder.toEntity(patientDTO);
            patient.setCaregiver(caregiver);
            patients.add(patient);
        }

        caregiver.setPatients(patients);
        return caregiver;
    }
}
