package com.builders;

import com.commons.dtos.MedicationIntakeDTO;
import com.entities.MedicationIntake;

public class MedicationIntakeBuilder {

    private MedicationIntakeBuilder() {
    }

    public static MedicationIntakeDTO toMedicationIntakeDTO(MedicationIntake medicationIntake) {
        MedicationIntakeDTO medicationIntakeDTO = new MedicationIntakeDTO(medicationIntake.getMedicationIntakeId(),
                medicationIntake.getMedication(), medicationIntake.getDate(), medicationIntake.getDailyInterval(),
                medicationIntake.getTaken());

        return medicationIntakeDTO;
    }

    public static MedicationIntake toEntity(MedicationIntakeDTO medicationIntakeDTO) {
        MedicationIntake medicationIntake = new MedicationIntake(medicationIntakeDTO.getMedication(),
                medicationIntakeDTO.getDate(), medicationIntakeDTO.getDailyInterval(), medicationIntakeDTO.getTaken());

        return medicationIntake;
    }
}
