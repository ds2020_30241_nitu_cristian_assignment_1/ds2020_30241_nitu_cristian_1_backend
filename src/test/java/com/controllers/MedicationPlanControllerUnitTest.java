package com.controllers;

import com.commons.dtos.MedicationPlanDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.services.MedicationPlanService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.Ds2020TestConfig;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class MedicationPlanControllerUnitTest extends Ds2020TestConfig {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicationPlanService service;

    @Test
    public void insertMedicationPlanTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationPlanDTO medicationPlanDTO = new MedicationPlanDTO("10-10-2020", "11-10-2020");

        mockMvc.perform(post("/medicationPlan")
                .content(objectMapper.writeValueAsString(medicationPlanDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

}
