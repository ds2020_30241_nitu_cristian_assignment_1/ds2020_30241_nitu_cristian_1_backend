package com.controllers;

import com.commons.dtos.MedicationDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.Ds2020TestConfig;
import com.services.MedicationService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class MedicationControllerUnitTest extends Ds2020TestConfig {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicationService service;

    @Test
    public void insertMedicationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationDTO medicationDTO = new MedicationDTO("name", 3, "SideEffect");

        mockMvc.perform(post("/medication")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

}
