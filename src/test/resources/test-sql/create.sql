INSERT INTO users (userId, username, password, role) values
    ('00201D1E61F3401588BF4292B86E22E4', 'My Name', 'My Address', 'Caregiver');

INSERT INTO medication (medicationId, dosage, name, sideEffect) values
    ('00201D1E61F3401588BF4292B86E2555', 3, 'MedicationName', 'SideEffectList');

INSERT INTO medicationPlan (medicationPlanId, endTreatment, startTreatment) values
    ('00201D1E61F3401588BF4292B86E2566', '22-02-2020', '21-02-2020');