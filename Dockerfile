FROM maven:3.6.3-jdk-11 AS builder

COPY ./src/ /root/src
COPY ./pom.xml /root/
COPY ./checkstyle.xml /root/
WORKDIR /root
RUN mvn package
RUN java -Djarmode=layertools -jar /root/target/ds2020_30241_nitu_cristian_1_backend-1.0-SNAPSHOT.jar list
RUN java -Djarmode=layertools -jar /root/target/ds2020_30241_nitu_cristian_1_backend-1.0-SNAPSHOT.jar extract
RUN ls -l /root

FROM openjdk:11.0.6-jre

ENV TZ=UTC
ENV DB_IP=ec2-52-206-15-227.compute-1.amazonaws.com
ENV DB_PORT=5432
ENV DB_USER=vidbnuagxmqmev
ENV DB_PASSWORD=b9710ef832c1c726d29839b317d5e8091a08590e6b76dd90110012a21e9eaa6b
ENV DB_DBNAME=d84trnolq081p2


COPY --from=builder /root/dependencies/ ./
COPY --from=builder /root/snapshot-dependencies/ ./

RUN sleep 10
COPY --from=builder /root/spring-boot-loader/ ./
COPY --from=builder /root/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher","-XX:+UseContainerSupport -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Xms512m -Xmx512m -XX:+UseG1GC -XX:+UseSerialGC -Xss512k -XX:MaxRAM=72m"]
